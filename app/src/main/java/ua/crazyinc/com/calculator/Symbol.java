package ua.crazyinc.com.calculator;

/**
 * Created by OLEG on 26.07.2016.
 */
public enum  Symbol {
    FIRST_DIGIT,
    OPERATION,
    SECOND_DIGIT
}
