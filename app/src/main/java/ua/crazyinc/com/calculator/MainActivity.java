package ua.crazyinc.com.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.EnumMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button button0, button1, button2, button3, button4, button5, button6,
            button7, button8, button9, buttonADD, buttonSubtract, buttonDevide,
            buttonMultiply, buttonResult, buttonComma, buttonClear, buttonBack, buttonMR, buttonMRC, buttonMRA, buttonMRS;
    TextView tvScreen, tvMemory;
    private OperationType operationType;
    private EnumMap<Symbol, Object> commands = new EnumMap<Symbol, Object>(Symbol.class);
    private boolean firstpressaftercalc = false;
    private boolean equalspressed;
    DecimalFormat df;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button0 = (Button) findViewById(R.id.button0);
        button0.setOnClickListener(this);
        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);
        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);
        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);
        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(this);
        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);
        button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(this);
        button7 = (Button) findViewById(R.id.button7);
        button7.setOnClickListener(this);
        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(this);
        button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(this);
        buttonADD = (Button) findViewById(R.id.buttonADD);
        buttonADD.setOnClickListener(this);
        buttonSubtract = (Button) findViewById(R.id.buttonSubtract);
        buttonSubtract.setOnClickListener(this);
        buttonMultiply = (Button) findViewById(R.id.buttonMultiply);
        buttonMultiply.setOnClickListener(this);
        buttonDevide = (Button) findViewById(R.id.buttonDevide);
        buttonDevide.setOnClickListener(this);
        buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(this);
        buttonComma = (Button) findViewById(R.id.buttonComma);
        buttonComma.setOnClickListener(this);
        buttonResult = (Button) findViewById(R.id.buttonResult);
        buttonResult.setOnClickListener(this);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);
        buttonMRC = (Button) findViewById(R.id.MRC);
        buttonMRC.setOnClickListener(this);
        buttonMRA = (Button) findViewById(R.id.MRA);
        buttonMRA.setOnClickListener(this);
        buttonMR = (Button) findViewById(R.id.MR);
        buttonMR.setOnClickListener(this);
        buttonMRS = (Button) findViewById(R.id.MRS);
        buttonMRS.setOnClickListener(this);

        buttonSubtract.setTag(OperationType.SUBTRUCT);
        buttonDevide.setTag(OperationType.DIVIDE);
        buttonADD.setTag(OperationType.ADD);
        buttonMultiply.setTag(OperationType.MULTIPLY);


        tvScreen = (TextView) findViewById(R.id.tvScreen);
        tvMemory = (TextView) findViewById(R.id.tvMR);

        df = new DecimalFormat("#.############################");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonMultiply:
            case R.id.buttonSubtract:
            case R.id.buttonADD:
            case R.id.buttonDevide: {
                operationType = (OperationType) view.getTag();
                if (firstpressaftercalc) {
                    commands.put(Symbol.OPERATION, operationType);
                } else {
                    firstpressaftercalc = true;
                    if (!commands.containsKey(Symbol.FIRST_DIGIT)) {
                        commands.put(Symbol.FIRST_DIGIT, tvScreen.getText());
                        commands.put(Symbol.OPERATION, operationType);
                    } else {
                        firstpressaftercalc = true;
                        commands.put(Symbol.SECOND_DIGIT, tvScreen.getText());
                        doCalc();
                        commands.put(Symbol.OPERATION, operationType);
                    }
                }
            }
            break;


            case R.id.button0:
            case R.id.button1:
            case R.id.button2:
            case R.id.button3:
            case R.id.button4:
            case R.id.button5:
            case R.id.button6:
            case R.id.button7:
            case R.id.button8:
            case R.id.button9: {
                if (firstpressaftercalc || tvScreen.getText().toString().equals("0")) {
                    tvScreen.setText(view.getContentDescription().toString());
                    firstpressaftercalc = false;
                } else {
                    tvScreen.setText(tvScreen.getText() + view.getContentDescription().toString());
                }
                commands.remove(Symbol.SECOND_DIGIT);
            }
            break;

            case R.id.buttonClear:
                commands.clear();
                tvScreen.setText("0");
                firstpressaftercalc = true;
                break;

            case R.id.buttonComma: {
                if (firstpressaftercalc) {
                    tvScreen.setText("0,");
                    firstpressaftercalc = false;
                } else if (tvScreen.getText().toString().contains(",") || (tvScreen.getText().toString().contains("."))) {

                } else {
                    tvScreen.setText(tvScreen.getText() + ",");
                    firstpressaftercalc = false;
                }
            }

            break;
            case R.id.buttonResult:
                if (commands.containsKey(Symbol.OPERATION)) {
                    if (commands.containsKey(Symbol.SECOND_DIGIT)) {
                        firstpressaftercalc = true;
                        doCalc();
                    } else {
                        commands.put(Symbol.SECOND_DIGIT, tvScreen.getText().toString());
                        firstpressaftercalc = true;
                        doCalc();
                    }
                    equalspressed = true;
                }
                break;
            case R.id.buttonBack:
                tvScreen.setText("0");
                break;
            case R.id.MR:
                if (!tvMemory.getText().toString().equals("memory")) {
                    tvScreen.setText(tvMemory.getText());
                    firstpressaftercalc = false;
                }
                break;
            case R.id.MRA:
                if (tvMemory.getText().toString().equals("memory")) {
                    tvMemory.setText(tvScreen.getText());
                } else {
                    tvMemory.setText(String.valueOf(df.format(getDouble(tvMemory.getText()) + getDouble(tvScreen.getText()))));
                }
                break;
            case R.id.MRS:
                if (tvMemory.getText().toString().equals("memory")) {
                    tvMemory.setText(tvScreen.getText());
                } else {
                    tvMemory.setText(String.valueOf(df.format(getDouble(tvMemory.getText()) - getDouble(tvScreen.getText()))));
                }
                break;
            case R.id.MRC:
                tvMemory.setText("memory");
                break;
        }
    }


    private void doCalc() {
        OperationType ot = (OperationType) commands.get(Symbol.OPERATION);
        double result = calc(ot,
                getDouble(commands.get(Symbol.FIRST_DIGIT)),
                getDouble(commands.get(Symbol.SECOND_DIGIT)));

        if (result % 1 == 0) {
            tvScreen.setText(String.valueOf((int) result));
        } else {

            tvScreen.setText(String.valueOf(df.format(result)));
        }
        commands.put(Symbol.FIRST_DIGIT, result);


    }

    private double getDouble(Object o) {
        double result = 0;
        try {
            result = Double.valueOf(o.toString().replace(',', '.'));

        } catch (Exception e) {
            result = 0;
        }
        return result;
    }


    private double calc(OperationType operationType, double a, double b) {
        double res = 0;
        switch (operationType) {
            case ADD:
                res = a + b;
                break;
            case DIVIDE:
                res = a / b;
                break;
            case MULTIPLY:
                res = a * b;
                break;
            case SUBTRUCT:
                res = a - b;
                break;
        }
        return res;
    }

}


